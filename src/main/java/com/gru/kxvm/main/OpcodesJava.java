package com.gru.kxvm.main;

public class OpcodesJava implements Op{
	
	public void execute(int opcode, Machine machine) {
		
		
		switch (opcode) {
		case 0x32:	aaload(machine); break;
//		case 0x53:	aastore(machine); break;
//		case 0x01: 	aconst_null(stack, heap); break;
//		case 0x19: 	aload(stack, heap); break;
//		case 0x2a:	aload_0(stack, heap); break;
//		case 0x2b: 	aload_1(stack, heap); break;
//		case 0x2c: 	aload_2(stack, heap); break;
//		case 0x2d: 	aload_3(stack, heap); break;
//		case 0xbd:	anewarray(stack, heap); break;
//		case 0xb0: 	areturn(stack, heap); break;
//		case 0xbe: 	arraylength(stack, heap); break;
//		case 0x3a:	astore(stack, heap); break;
//		case 0x4b:	astore_0(stack, heap); break;
//		case 0x4c:	astore_1(stack, heap); break;
//		case 0x4d:	astore_2(stack, heap); break;
//		case 0x4e:	astore_3(stack, heap); break;
//		case 0xbf:	athrow(stack, heap); break;
//		case 0x33:	baload(stack, heap); break;
//		case 0x54:	bastore(stack, heap); break;
//		case 0x10:	bipush(stack, heap); break;
//		case 0xca:	breakpoint(stack, heap); break;
//		case 0x34:	caload(stack, heap); break;
//		case 0x55:	castore(stack, heap); break;
//		case 0xc0:	checkcast(stack, heap); break;
//		case 0x90:	d2f(stack, heap); break;
//		case 0x8e:	d2i(stack, heap); break;
//		case 0x8f:	d2l(stack, heap); break;
//		case 0x63:	dadd(stack, heap); break;
//		case 0x31:	daload(stack, heap); break;
//		case 0x52:	dastore(stack, heap); break;
//		case 0x98:	dcmpg(stack, heap); break;
//		case 0x97:	dcmpl(stack, heap); break;
//		case 0x0e:	dconst_0(stack, heap); break;
//		case 0x0f:	dconst_1(stack, heap); break;
//		case 0x6f: 	ddiv(stack, heap); break;
//		case 0x18:	dload(stack, heap); break;
//		case 0x26:	dload_0(stack, heap); break;
//		case 0x27:	dload_1(stack, heap); break;
//		case 0x28:	dload_2(stack, heap); break;
//		case 0x29:	dload_3(stack, heap); break;
//		case 0x6b:	dmul(stack, heap); break;
//		case 0x77:	dneg(stack, heap); break;
//		case 0x73:	drem(stack, heap); break;
//		case 0xaf:	dreturn(stack, heap); break;
//		case 0x39:	dstore(stack, heap); break;
//		case 0x47:	dstore_0(stack, heap); break;
//		case 0x48:	dstore_1(stack, heap); break;
//		case 0x49:	dstore_2(stack, heap); break;
//		case 0x4a:	dstore_3(stack, heap); break;
//		case 0x67:	dsub(stack, heap); break;
//		case 0x59:	dup(stack, heap); break;
//		case 0x5a:	dup_x1(stack, heap); break;
//		case 0x5b:	dup_x2(stack, heap); break;
//		case 0x5c:	dup2(stack, heap); break;
//		case 0x5d:	dup2_x1(stack, heap); break;
//		case 0x5e:	dup2_x2(stack, heap); break;
//		case 0x8d:	f2d(stack, heap); break;
//		case 0x8b:	f2i(stack, heap); break;
//		case 0x8c:	f2l(stack, heap); break;
//		case 0x62:	fadd(stack, heap); break;
//		case 0x30:	faload(stack, heap); break;
//		case 0x51:	fastore(stack, heap); break;
//		case 0x96:	fcmpg(stack, heap); break;
//		case 0x95:	fcmpl(stack, heap); break;
//		case 0x0b:	fconst_0(stack, heap); break;
//		case 0x0c:	fconst_1(stack, heap); break;
//		case 0x0d:	fconst_2(stack, heap); break;
//		case 0x6e:	fdiv(stack, heap); break;
//		case 0x17:	fload(stack, heap); break;
//		case 0x22:	fload_0(stack, heap); break;
//		case 0x23:	fload_1(stack, heap); break;
//		case 0x24:	fload_2(stack, heap); break;
//		case 0x25:	fload_3(stack, heap); break;
//		case 0x6a:	fmul(stack, heap); break;
//		case 0x76:	fneg(stack, heap); break;
//		case 0x72:	frem(stack, heap); break;
//		case 0xae:	freturn(stack, heap); break;
//		case 0x38:	fstore(stack, heap); break;
//		case 0x43:	fstore_0(stack, heap); break;
//		case 0x44:	fstore_1(stack, heap); break;
//		case 0x45:	fstore_2(stack, heap); break;
//		case 0x46:	fstore_3(stack, heap); break;
//		case 0x66:	fsub(stack, heap); break;
//		case 0xb4:	getfield(stack, heap); break;
//		case 0xb2:	gestatic(stack, heap); break;
//		case 0xa7:	_goto(stack, heap); break;
//		case 0xc8:	goto_w(stack, heap); break;
//		case 0x91:	i2b(stack, heap); break;
//		case 0x92:	i2c(stack, heap); break;
//		case 0x87:	i2d(stack, heap); break;
//		case 0x86:	i2f(stack, heap); break;
//		case 0x85:	i2l(stack, heap); break;
//		case 0x93:	i2s(stack, heap); break;
//		case 0x60:	iadd(stack, heap); break;
//		case 0x2e:	iaload(stack, heap); break;
//		case 0x7e: 	iand(stack, heap); break;
//		case 0x4f:	iastore(stack, heap); break;
//		case 0x02:	iconst_m1(stack, heap); break;
//		case 0x03:	iconst_0(stack, heap); break;
//		case 0x04:	iconst_1(stack, heap); break;
//		case 0x05:	iconst_2(stack, heap); break;
//		case 0x06:	iconst_3(stack, heap); break;
//		case 0x07:	iconst_4(stack, heap); break;
//		case 0x08:	iconst_5(stack, heap); break;
//		case 0x6c:	idiv(stack, heap); break;
//		case 0xa5:	if_acmpeq(stack, heap); break;
//		case 0xa6:	if_acmpne(stack, heap); break;
//		case 0x9f:	ificmpeq(stack, heap); break;
//		case 0xa2:	ificmpge(stack, heap); break;
//		case 0xa3:	if_icmpgt(stack, heap); break;
//		case 0xa4:	if_icmple(stack, heap); break;
//		case 0xa1:	if_icmpit(stack, heap); break;
//		case 0xa0:	if_icmpne(stack, heap); break;
//		case 0x9c:	ifge(stack, heap); break;
//		case 0x9d:	ifgt(stack, heap); break;
//		case 0x9e:	ifle(stack, heap); break;
//		case 0x9b:	iflt(stack, heap); break;
//		case 0x9a:	ifne(stack, heap); break;
//		case 0xc7:	ifnonnull(stack, heap); break;
//		case 0xc6:	ifnull(stack, heap); break;
//		case 0x84:	iinc(stack, heap); break;
//		case 0x15:	iload(stack, heap); break;
//		case 0x1a:	iload_0(stack, heap); break;
//		case 0x1b:	iload_1(stack, heap); break;
//		case 0x1c:	iload_2(stack, heap); break;
//		case 0x1d: 	iload_3(stack, heap); break;
//		case 0xfe:	impdep1(stack, heap); break;
//		case 0xff:	impdep2(stack, heap); break;
//		case 0x68:	imul(stack, heap); break;
//		case 0x74:	ineg(stack, heap); break;
//		case 0xc1:	_instanceof(stack, heap); break;
//		case 0xba:	invokedynamic(stack, heap); break;
//		case 0xb9:	invokeinterface(stack, heap); break;
//		case 0xb7:	invokespecial(stack, heap); break;
//		case 0xb8:	invokestatic(stack, heap); break;
//		case 0xb6:	invokevirtual(stack, heap); break;
//		case 0x80:	ior(stack, heap); break;
//		case 0x70:	irem(stack, heap); break;
//		case 0xac:	ireturn(stack, heap); break;
//		case 0x78:	ishl(stack, heap); break;
//		case 0x7a:	ishr(stack, heap); break;
//		case 0x36:	istore(stack, heap); break;
//		case 0x3b:	istore_0(stack, heap); break;
//		case 0x3c:	istore_1(stack, heap); break;
//		case 0x3d:	istore_2(stack, heap); break;
//		case 0x3e:	istore_3(stack, heap); break;
//		case 0x64:	isub(stack, heap); break;
//		case 0x7c:	iush(stack, heap); break;
//		case 0x82:	ixor(stack, heap); break;
//		case 0xa8:	jsr(stack, heap); break;
//		case 0xc9:	jsr_w(stack, heap); break;
//		case 0x8a:	l2d(stack, heap); break;
//		case 0x89:	l2f(stack, heap); break;
//		case 0x88:	l2i(stack, heap); break;
//		case 0x61:	ladd(stack, heap); break;
//		case 0x2f:	laload(stack, heap); break;
//		case 0x7f:	land(stack, heap); break;
//		case 0x50:	lastore(stack, heap); break;
//		case 0x94:	lcmp(stack, heap); break;
//		case 0x09:	lconst_0(stack, heap); break;
//		case 0x0a:	lconst_1(stack, heap); break;
//		case 0x12:	ldc(stack, heap); break;
//		case 0x13:	ldc_w(stack, heap); break;
//		case 0x14:	ldc2_w(stack, heap); break;
//		case 0x6d:	ldiv(stack, heap); break;
//		case 0x16:	lload(stack, heap); break;
//		case 0x1e:	lload_0(stack, heap); break;
//		case 0x1f:	lload_1(stack, heap); break;
//		case 0x20:	lload_2(stack, heap); break;
//		case 0x21:	lload_3(stack, heap); break;
//		case 0x69:	lmul(stack, heap); break;
//		case 0x75:	lneg(stack, heap); break;
//		case 0xab:	lookupswitch(stack, heap);  break;
//		case 0x81:	lor(stack, heap);  break;
//		case 0x71:	lrem(stack, heap); break;
//		case 0xad:	lreturn(stack, heap); break;
//		case 0x79:	lshl(stack, heap); break;
//		case 0x7b:	lshr(stack, heap); break;
//		case 0x37:	lstore(stack, heap); break;
//		case 0x3f:	lstore_0(stack, heap); break;
//		case 0x40:	lstore_1(stack, heap); break;
//		case 0x41:	lstore_2(stack, heap); break;
//		case 0x42:	lstore_3(stack, heap); break;
//		case 0x65:	lsub(stack, heap); break;
//		case 0x7d:	lushr(stack, heap); break;
//		case 0x83:	lxor(stack, heap); break;
//		case 0xc2:	monitorenter(stack, heap);  break;
//		case 0xc3:	monitorexit(stack, heap); break;
//		case 0xc5:	multianewarray(stack, heap); break;
//		case 0xbb:	_new(stack, heap); break;
//		case 0xbc:	newarray(stack, heap); break;
//		case 0x00: 	nop(stack, heap); break;
//		case 0x57:	pop(stack, heap); break;
//		case 0x58:	pop2(stack, heap); break;
//		case 0xb5:	putfield(stack, heap); break;
//		case 0xb3:	putstatic(stack, heap); break;
//		case 0xa9:	ret(stack, heap); break;
//		case 0xb1:	_return(stack, heap); break;
//		case 0x35:	saload(stack, heap); break;
//		case 0x11:	sipush(stack, heap); break;
//		case 0x5f:	swap(stack, heap); break;
//		case 0xaa:	tableswitch(stack, heap); break;
//		case 0xc4:	wide(stack, heap); break;
		default:  break;
		
		}
		
	}
	
	
	private void wide(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void tableswitch(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void swap(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void sipush(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void saload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void _return(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ret(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void putstatic(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void putfield(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void pop2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void pop(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void nop(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void newarray(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void _new(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void multianewarray(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void monitorexit(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void monitorenter(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lxor(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lushr(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lsub(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lstore_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lstore_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lstore_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lstore_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lstore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lshr(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lshl(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lreturn(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lrem(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lor(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lookupswitch(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lneg(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lmul(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lload_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lload_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lload_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lload_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ldiv(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ldc2_w(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ldc_w(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ldc(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lconst_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lconst_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lcmp(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void lastore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void land(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void laload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ladd(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void l2i(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void l2f(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void l2d(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void jsr_w(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void jsr(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ixor(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iush(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void isub(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void istore_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void istore_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void istore_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void istore_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void istore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ishr(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ishl(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ireturn(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void irem(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ior(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void invokevirtual(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void invokestatic(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void invokespecial(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void invokeinterface(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void invokedynamic(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void _instanceof(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ineg(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void imul(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void impdep2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void impdep1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iload_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iload_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iload_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iload_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iinc(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ifnull(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ifnonnull(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ifne(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iflt(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ifle(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ifgt(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ifge(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void if_icmpne(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void if_icmpit(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void if_icmple(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void if_icmpgt(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ificmpge(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ificmpeq(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void if_acmpne(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void if_acmpeq(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void idiv(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iconst_5(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iconst_4(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iconst_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iconst_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iconst_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iconst_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iconst_m1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iastore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iand(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iaload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void iadd(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void i2s(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void i2l(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void i2f(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void i2d(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void i2c(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void i2b(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void goto_w(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void _goto(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void gestatic(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fsub(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void getfield(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fstore_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fstore_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fstore_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fstore_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fstore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void freturn(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void frem(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fneg(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fmul(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fload_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fload_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fload_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fload_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fdiv(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fconst_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fconst_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fconst_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fcmpl(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fcmpg(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fastore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void faload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void fadd(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void f2l(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void f2i(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void f2d(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dup2_x2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dup2_x1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dup2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dup_x2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dup_x1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dup(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dsub(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dstore_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dstore_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dstore_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dstore_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dstore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dreturn(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void drem(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dneg(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dmul(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dload_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dload_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dload_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dload_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void ddiv(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dconst_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dconst_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dcmpl(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dcmpg(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dastore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void daload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void dadd(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void d2l(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void d2i(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void d2f(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void checkcast(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void castore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void caload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void breakpoint(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void bipush(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void bastore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void baload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void athrow(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void astore_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void astore_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void astore_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void astore_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void astore(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void arraylength(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void areturn(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void anewarray(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void aload_3(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void aload_2(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void aload_1(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void aload_0(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void aload(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	private void aconst_null(byte[] stack, byte[] heap) {
		// TODO Auto-generated method stub
		
	}


	public void aaload(Machine machine) {
		int index = machine.stack[machine.sp--];
		int arrayref = machine.stack[machine.sp--];
		int value = machine.heap[arrayref + index];
		machine.stack[machine.sp] = value;
		
		// TODO checks and throws on error
		
	}
	
	public void aastore(byte[] stack, byte[] heap) {
		
	}

}
