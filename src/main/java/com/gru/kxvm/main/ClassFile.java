package com.gru.kxvm.main;

public class ClassFile {
	
	public String magic;
	public int minorVersion;
	public int majorVersion;
	public int constantPoolCount;
	
	public static int CONSTANT_Class = 7;
	public static int CONSTANT_Fieldref = 9;
	public static int CONSTANT_Methodref = 10;
	public static int CONSTANT_InterfaceMethodref = 11;
	public static int CONSTANT_String = 8;
	public static int CONSTANT_Integer = 3;
	public static int CONSTANT_Float = 4;
	public static int CONSTANT_Long = 5;
	public static int CONSTANT_Double = 6;
	public static int CONSTANT_NameAndType = 12;
	public static int CONSTANT_Utf8	= 1;
	public static int CONSTANT_MethodHandle	=15;
	public static int CONSTANT_MethodType =	16;
	public static int CONSTANT_Dynamic = 17;
	public static int CONSTANT_InvokeDynamic = 18;
	public static int CONSTANT_Module = 19;
	public static int CONSTANT_Package = 20;
	
	public static class CP_Info {
		public int tag;
	}
	
	
	public static class ConstantPoolInfo {
		int tag;
		int[] subInfos;
	}
	
	public static class CP_ClassInfo extends CP_Info {
	
		public int nameIndex;
		
	}

	public static class CP_FieldMethodInterfaceRefInfo extends CP_Info {
	
		public int classIndex;
		public int nameAndTypeIndex;
	}
	
	public static class CP_StringInfo extends CP_Info {
	
		public int stringIndex;
	}
	
	public static class CP_IntegerFloatInfo extends CP_Info {
		
		public byte[] bytes;
	}
	
	
	public static class CP_LongDoubleInfo extends CP_Info {
		public byte[] highBytes;
		public byte[] lowBytes;
	}
	
	
	public static class CP_UTF8Info extends CP_Info {
		public int length;
		public byte[] bytes;
	}
	
	public static class CP_MethodHandleInfo extends CP_Info {
		public static int refKind;
		public static int refIndex;
	}
	
	public static class CP_MethodTypeInfo extends CP_Info {
		public int descriptorIndex;
	}
	
	public static class CP_DynamicInfo extends CP_Info {
		public int bootstrapMethodAttrIndex;
		public int nameAndTypeIndex;
	}
	
	public static class CP_ModulePackageInfo extends CP_Info {
		public int nameIndex;
	}
	
	public static class CP_NameAndTypeInfo extends CP_Info{
		public int name_index;
	    public int descriptor_index;
	}
	

}
