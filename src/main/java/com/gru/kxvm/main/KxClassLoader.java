package com.gru.kxvm.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.gru.kxvm.main.ClassFile.CP_ClassInfo;
import com.gru.kxvm.main.ClassFile.CP_DynamicInfo;
import com.gru.kxvm.main.ClassFile.CP_FieldMethodInterfaceRefInfo;
import com.gru.kxvm.main.ClassFile.CP_Info;
import com.gru.kxvm.main.ClassFile.CP_IntegerFloatInfo;
import com.gru.kxvm.main.ClassFile.CP_LongDoubleInfo;
import com.gru.kxvm.main.ClassFile.CP_MethodHandleInfo;
import com.gru.kxvm.main.ClassFile.CP_MethodTypeInfo;
import com.gru.kxvm.main.ClassFile.CP_ModulePackageInfo;
import com.gru.kxvm.main.ClassFile.CP_NameAndTypeInfo;
import com.gru.kxvm.main.ClassFile.CP_StringInfo;
import com.gru.kxvm.main.ClassFile.CP_UTF8Info;

/*
 
 see https://docs.oracle.com/javase/specs/jvms/se12/html/jvms-4.html
 ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
  
 */

public class KxClassLoader {
	
	List<CP_Info> constantPool = new ArrayList<CP_Info>();
	
	private int intFromWord(byte[] word) {
		return word[0] << 8 | word[1];
	}
	
	private int intFromQuad(byte[] word) {
		return word[0] << 24 | word[1] << 16 | word[2] << 8 | word[3] ;
	}
	
	private void readConstMethodRef(byte[] data) {
		byte[] classIndexB = new byte[2];
		classIndexB[0] = data[0];
		classIndexB[1] = data[1];
		int classIndex = intFromWord(classIndexB);
		
		byte[] nameAndTypeIndexB = new byte[2];
		nameAndTypeIndexB[0] = data[2];
		nameAndTypeIndexB[1] = data[3];
		int nameIndex = intFromWord(nameAndTypeIndexB);
		
		System.out.println("MethodRef: ");
		System.out.println("\tclassIndex: " + classIndex);
		System.out.println("\tnaneIndex: " + nameIndex);
		CP_FieldMethodInterfaceRefInfo info = new CP_FieldMethodInterfaceRefInfo();
		info.classIndex = classIndex;
		info.nameAndTypeIndex = nameIndex;
		constantPool.add(info);
		
	}
	
	private void readConstClassRef(byte[] data) {
		byte[] bs = {data[0], data[1]};
		int nameIndex = intFromWord(bs);
		System.out.println("ClassRef:");
		System.out.println("\tnameIndex: " + nameIndex);
		CP_ClassInfo cpClassInfo = new CP_ClassInfo();
		cpClassInfo.nameIndex = nameIndex;
		constantPool.add(cpClassInfo);
	}
	
	private void readConstFieldRef(byte[] data) {
		CP_FieldMethodInterfaceRefInfo info = new CP_FieldMethodInterfaceRefInfo();
		info.classIndex = intFromWord(new byte[] { data[0], data[1]});
		info.nameAndTypeIndex = intFromWord(new byte[] { data[2], data[3]});
		constantPool.add(info);
	}
	
	private String getStringFromConstantPool(int index) {
		int realIndex = index -1;
		CP_UTF8Info info = (CP_UTF8Info)constantPool.get(realIndex);
		return new String(info.bytes);
	}
	
	private int getNumberOfBytesForCPTag(int tag, FileInputStream fis) throws IOException {
		switch (tag) {
		case 1: {
			return intFromWord(fis.readNBytes(2));
		}
		case 3: 
		case 4: return 4;
		case 5:
		case 6: return 8;
		case 7: return 2;
		case 8: return 2;
		case 9: return 4;
		case 10: return 4;
		case 11: return 4;
		case 12: return 4;
		case 15: return 4;
		case 16: return 2;
		case 17: return 4;
		case 18: return 4;
		case 19: return 2;
		case 20: return 2;
		
		default: return 0;
		}
	}
	
	public void loadClass(String name) {
		try {
			FileInputStream fis = new FileInputStream(name);
			byte[] magic = fis.readNBytes(4);
			byte[] minorBytes = fis.readNBytes(2);
			byte[] majorBytes = fis.readNBytes(2);
			int minorV = intFromWord(minorBytes);
			int majorV = intFromWord(majorBytes);
			
			// Constant pool 
			{
				int cpCount = intFromWord(fis.readNBytes(2));
				for (int i=0; i < cpCount-1; i++) {
					int tag = (fis.readNBytes(1))[0];
					int cpInfoByteNumber = getNumberOfBytesForCPTag(tag, fis);
					byte[] cpInfoBytes = fis.readNBytes(cpInfoByteNumber);
					
					switch(tag) {
					case 1: readConstUTF8(cpInfoBytes); break;
					case 3: readConstInt(cpInfoBytes); break;
					case 4: readConstFloat(cpInfoBytes); break;
					case 5: readConstLong(cpInfoBytes); break;
					case 6: readConstDouble(cpInfoBytes); break;
					case 7: readConstClassRef(cpInfoBytes); break;
					case 8: readConstString(cpInfoBytes); break;
					case 9: readConstFieldRef(cpInfoBytes); break;
					case 10: readConstMethodRef(cpInfoBytes); break;
					case 11: readConstInterfaceRef(cpInfoBytes); break;
					case 12: readConstNameAndType(cpInfoBytes); break;
					case 15: readConstMethodHandle(cpInfoBytes); break;
					case 16: readConstMethodType(cpInfoBytes); break;
					case 17: readConstDynamic(cpInfoBytes); break;
					case 18: readConstInvokeDynamic(cpInfoBytes); break;
					case 19: readConstModule(cpInfoBytes); break;
					case 20: readConstPackage(cpInfoBytes); break;
					default: throw new RuntimeException("unkwon constant pool tag!" + tag);
					}
					
				}
				// Access flags
				{
					int accessFlags = intFromWord(fis.readNBytes(2));
					readAccessFlags(accessFlags);
				}
				
				// this class
				{
					int thisClass = intFromWord(fis.readNBytes(2));
					dumpClassName(thisClass, "this class");
				}
				
				// super class
				{
					int superClass = intFromWord(fis.readNBytes(2));
					dumpClassName(superClass, "super class");
				}
				
				// interfaces count
				{
					int interfacesCount = intFromWord(fis.readNBytes(2));
					System.out.println("Interfaces Count: " + interfacesCount);
					
					// interfaces
					// TODO actually read the interfaces
					fis.readNBytes(interfacesCount  * 2);
				}
				
				// fields
				{
					int fieldCount = intFromWord(fis.readNBytes(2));
					System.out.println("number of fields: "  + fieldCount);
					
					for (int i = 0; i< fieldCount; i++ ) {
						/*
						  field_info {
						 
						    u2             access_flags;
						    u2             name_index;
						    u2             descriptor_index;
						    u2             attributes_count;
						    attribute_info attributes[attributes_count];
						}
						*/
						// TODO read field info
						int accessFlags = intFromWord(fis.readNBytes(2));
						int nameIndex = intFromWord(fis.readNBytes(2));
						System.out.println("field name: " + getStringFromConstantPool(nameIndex));
						System.out.println("\tfield access flags: " + accessFlags);
						int descIndex = intFromWord(fis.readNBytes(2));
						int attCount = intFromWord(fis.readNBytes(2));
						
						for (int a = 0; a < attCount; a++) {
							/*
							 	attribute_info {
								    u2 attribute_name_index;
								    u4 attribute_length;
								    u1 info[attribute_length];
								}
							 */
							
							int attNameIndex = intFromWord(fis.readNBytes(2));
							int attLen = intFromQuad(fis.readNBytes(4));
							for (int al = 0; al < attLen; al++) {
								int attInfo = fis.read();
							}
							
						}

						
					}
				}
				
				
				// methods
				{
					int methodsCount = intFromWord(fis.readNBytes(2));
					for (int i = 0; i< methodsCount; i++) {
						/*
						 method_info {
						    u2             access_flags;
						    u2             name_index;
						    u2             descriptor_index;
						    u2             attributes_count;
						    attribute_info attributes[attributes_count];
						 }
						 */
						
						int accFlags = intFromWord(fis.readNBytes(2));
						
						int nameIndex = intFromWord(fis.readNBytes(2));
						System.out.println("Method name: " + getStringFromConstantPool(nameIndex));
						System.out.println("\tMethod access flags: " + accFlags);
						int descIndex = intFromWord(fis.readNBytes(2));
						int attCount = intFromWord(fis.readNBytes(2));
						for (int a = 0; a < attCount; a++) {
							/*
							 	attribute_info {
								    u2 attribute_name_index;
								    u4 attribute_length;
								    u1 info[attribute_length];
								}
							 */
							
							int attNameIndex = intFromWord(fis.readNBytes(2));
							int attLen = intFromQuad(fis.readNBytes(4));
							System.out.println("Method attribute Name: " + getStringFromConstantPool(attNameIndex));
							for (int al = 0; al < attLen; al++) {
								int attInfo = fis.read();
								System.out.println("method attInfo: " + attInfo);
							}
							
						}

 
					}
				}
				
				// attributes 
				{
					int attCount = intFromWord(fis.readNBytes(2));
					for (int ai = 0; ai < attCount; ai++) {
						/*
					 	attribute_info {
						    u2 attribute_name_index;
						    u4 attribute_length;
						    u1 info[attribute_length];
						}
					 */
					
					int attNameIndex = intFromWord(fis.readNBytes(2));
					System.out.println("Att Name: " + getStringFromConstantPool(attNameIndex));
					int attLen = intFromQuad(fis.readNBytes(4));
					for (int al = 0; al < attLen; al++) {
						int attInfo = fis.read();
					}
					}
				}

			}
			
			
			System.out.println("Format version: " + majorV + "." + minorV);
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void dumpClassName(int thisClass, String typeOfClass) {
		// From jvm docs: 
		//The value of the this_class item must be a valid index 
		// into the constant_pool table. 
		// The constant_pool entry at that index must be a 
		// CONSTANT_Class_info structure (�4.4.1) 
		// representing the class or interface defined by this class file.
		// The constant pool is indexed from 1 to count - 1
		CP_Info cpInfo = constantPool.get(thisClass-1);
		if (cpInfo instanceof CP_ClassInfo) {
			int nameIndex = ((CP_ClassInfo)cpInfo).nameIndex;
			System.out.println(typeOfClass + " nameIndex: " + nameIndex);
			String name = new String(((CP_UTF8Info) constantPool.get(nameIndex-1)).bytes);
			System.out.println(typeOfClass + " name: " + name);
		}
	}

	private void readAccessFlags(int flags) {
		System.out.println("Access Flags:");
		System.out.print("\t");
		switch (flags) {
		case 0x0001: System.out.println("ACC PUBLIC");break;
		case 0x0010: System.out.println("ACC_FINAL"); break;
		case 0x0020: System.out.println("ACC_SUPER"); break;
		case 0x0200: System.out.println("ACC_INTERFACE"); break;
		case 0x0400: System.out.println("ACC_ABSTRACT"); break;
		case 0x1000: System.out.println("ACC_SYNTHETIC"); break;
		case 0x2000: System.out.println("ACC_ANNOTATION"); break;
		case 0x4000: System.out.println("ACC_ENUM"); break;
		case 0x8000: System.out.println("ACC_MODULE"); break;
		default: System.out.println("No ACCESS Flags set!");
		}
	}

	private void readConstPackage(byte[] cpInfoBytes) {
		System.out.println("Package:");
		CP_ModulePackageInfo info = new CP_ModulePackageInfo();
		info.nameIndex = intFromWord(cpInfoBytes);
		constantPool.add(info);
	}

	private void readConstInvokeDynamic(byte[] cpInfoBytes) {
		System.out.println("InvokeDynamic:");
		CP_DynamicInfo info = new CP_DynamicInfo();
		info.bootstrapMethodAttrIndex = intFromWord(new byte[] {cpInfoBytes[0], cpInfoBytes[1]});
		info.nameAndTypeIndex = intFromWord(new byte[]  {cpInfoBytes[2], cpInfoBytes[3]});
		constantPool.add(info);
	}

	private void readConstModule(byte[] cpInfoBytes) {
		System.out.println("Module:");
		CP_ModulePackageInfo info = new CP_ModulePackageInfo(); 
		info.nameIndex = intFromWord(cpInfoBytes);
		constantPool.add(info);
	}

	private void readConstDynamic(byte[] cpInfoBytes) {
		System.out.println("Dynamic:");
		readConstInvokeDynamic(cpInfoBytes);
	}

	private void readConstMethodType(byte[] cpInfoBytes) {
		System.out.println("MethodType:");
		CP_MethodTypeInfo info = new CP_MethodTypeInfo();
		info.descriptorIndex = intFromWord(cpInfoBytes);
		constantPool.add(info);
	}

	private void readConstMethodHandle(byte[] cpInfoBytes) {
		System.out.println("MethodHandle:");
		CP_MethodHandleInfo info = new CP_MethodHandleInfo();
		info.refKind = intFromWord(new byte[] {cpInfoBytes[0], cpInfoBytes[1]});
		info.refIndex = intFromWord(new byte[] { cpInfoBytes[2], cpInfoBytes[3]});
		constantPool.add(info);
	
	}

	private void readConstNameAndType(byte[] cpInfoBytes) {
		System.out.println("NameAndType:");
		CP_NameAndTypeInfo info = new CP_NameAndTypeInfo();
		info.name_index = intFromWord(new byte[] { cpInfoBytes[0], cpInfoBytes[1]});
		info.descriptor_index = intFromWord(new byte[] { cpInfoBytes[2], cpInfoBytes[3]});
		constantPool.add(info);
	}

	private void readConstString(byte[] cpInfoBytes) {
		System.out.println("Const string:");
		CP_StringInfo info = new CP_StringInfo();
		info.stringIndex = intFromWord(cpInfoBytes);
		constantPool.add(info);
	}

	private void readConstDouble(byte[] cpInfoBytes) {
		CP_LongDoubleInfo info = new CP_LongDoubleInfo();
		info.highBytes = new byte[] { cpInfoBytes[0], cpInfoBytes[1], cpInfoBytes[2], cpInfoBytes[3]};
		info.lowBytes = new byte[] { cpInfoBytes[4], cpInfoBytes[5], cpInfoBytes[6], cpInfoBytes[7]};
		constantPool.add(info);
	}

	private void readConstLong(byte[] cpInfoBytes) {
		readConstDouble(cpInfoBytes);
	}

	private void readConstFloat(byte[] cpInfoBytes) {
		readConstInt(cpInfoBytes);
		
	}

	private void readConstInt(byte[] cpInfoBytes) {
		CP_IntegerFloatInfo info = new CP_IntegerFloatInfo();
		info.bytes = cpInfoBytes;
		constantPool.add(info);
		
	}

	private void readConstUTF8(byte[] cpInfoBytes) {
		CP_UTF8Info cpinfo = new CP_UTF8Info();
		cpinfo.bytes = cpInfoBytes;
		cpinfo.length = cpInfoBytes.length;
		constantPool.add(cpinfo);
		
		System.out.println("Const utf8:");
		System.out.println("\t" + new String(cpInfoBytes));
		
	}

	private void readConstInterfaceRef(byte[] cpInfoBytes) {
		System.out.println("Const interface:");
		
		
	}

}
