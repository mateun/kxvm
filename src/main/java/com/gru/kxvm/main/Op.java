package com.gru.kxvm.main;

public interface Op {
	
	void execute(int opcode, Machine machine);

}
